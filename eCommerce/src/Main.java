import java.io.*;
import java.util.*;

class Inventory {
    float price;
    String item;
    String item_d;
}

class Customer {
    float currency;
    String name;
    Cart[] sCart = new Cart[5];
}

class Cart {
    float iPrice;
    String iName;
}

public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        //-------------------------------------------------------------------
        // Here is the declaration of all initialized variables.
        // There is no pass by reference, so this'll be a trip.
        //-------------------------------------------------------------------

        Scanner Read = new Scanner(System.in);
        // Read in from keyboard.
        File file = new File("/Users/xaxp185/Desktop/shopping/eCommerce/Inventory.txt");
        // In order to be properly parsed, you must use \\ for file locations.
        Scanner fileRead = new Scanner(file);
        // Create a scanner to read this specific file.

        char cmd;
        int inc = 0;
        final int MAX = 15; // "final" is the keyword for constant.
        String fName, lName;

        Customer cust = new Customer();
        Inventory[] ItemsForSale = new Inventory[MAX]; // Creates an array that holds 15 Inventory objects.
        for (int i = 0; i < ItemsForSale.length; i++) {
            ItemsForSale[i] = new Inventory(); // Initializes these 15 Inventory objects.
        }

        for (int i = 0; i < cust.sCart.length; i++) {
            cust.sCart[i] = new Cart(); // Initializing customer's 5 cart space.
        }

        //-------------------------------------------------------------------
        // This is a simple program used to emulate purchasing
        // and checking out items from a store. Used to learn Java.
        //-------------------------------------------------------------------

        System.out.println(" ");

        System.out.println("*** Welcome to 'Stables and Poormen'! ***"); // System.out.println is the equivalent of a string literal statement + cout.
        // Dead ass just making fun of Barnes and Nobles.
        System.out.println("Please enter your first and last name:");

        //-------------------------------------------------------------------
        // Read in all inventory items from a file.
        // Do it by price, item, and description.
        // Do not allow it to overfill the array.
        //-------------------------------------------------------------------

        while (fileRead.hasNextLine() && inc < MAX) {

            ItemsForSale[inc].price = fileRead.nextFloat(); // Read the next float in the file.
            ItemsForSale[inc].item = fileRead.next(); // Read the next word in the file.
            ItemsForSale[inc].item_d = fileRead.nextLine(); // Try to grab the entire remainder of the description.

            inc++;
        }

        //-------------------------------------------------------------------
        // Get user input.
        // This will be used to make an 'account' for them.
        //-------------------------------------------------------------------

        fName = Read.next(); // This reads the next word.
        lName = Read.next(); // Alternatively, to split by whitespace, you can use line.split(" ").

        //-------------------------------------------------------------------
        // Format the greeting and begin the menu process.
        //-------------------------------------------------------------------

        cust.name = String.format("%s %s", fName, lName); // String format is used to fill a string variable.

        System.out.println("How much will you be willing to spend, today, " + cust.name + "?");
        cust.currency = Read.nextFloat();

        System.out.println("Thanks, " + cust.name + "!" + " Please enjoy your stay."); // You can concatenate in these, similar to cout.
        System.out.println(" "); // the ln in println means "line". You can omit it to do it on a continuous line.

        //-------------------------------------------------------------------
        // Begin the menu for my eCommerce.
        //-------------------------------------------------------------------

        MainMenu();

        cmd = Read.next().charAt(0); // Take the first character read in from a "string".
        cmd = Character.toUpperCase(cmd); // Convert said character to upper case.

        //-------------------------------------------------------------------
        // While + Switch statements to have the program running as long as
        // necessary for user to make purchases.
        //-------------------------------------------------------------------

        while (cmd != 'Q') {

            switch (cmd) { // Switch statements exist in Java.

                case '1' : // Inventory
                    InvCmd(cust, ItemsForSale, Read);
                    break;

                case '2' : // Shopping Cart
                    break;

                case '3' : // Receipt
                    break;

                default :
                    System.out.println("That is not a proper option.");
                    System.out.println("Please try again.");
            } // Switch for Menu

            System.out.println(" ");
            MainMenu();
            cmd = Read.next().charAt(0);
            cmd = Character.toUpperCase(cmd);

        } // While for Menu
    } // Menu main.

    //-------------------------------------------------------------------
    // The void method to repeat the main menu when needed.
    //-------------------------------------------------------------------

    private static void MainMenu () {

        System.out.println("********** STORE FRONT **********");
        System.out.println("OPTION 1:   INVENTORY");
        System.out.println("OPTION 2:   MY CART");
        System.out.println("OPTION 3:   RECEIPT");
        System.out.println("OPTION Q:   SAVE AND EXIT");

        System.out.println(" ");
        System.out.println("Please enter a command.");

    } // Main Menu Void

    //-------------------------------------------------------------------
    // Everything below is for the Inventory branch.
    // Methods to utilize the "Inventory" option.
    //-------------------------------------------------------------------

    private static void InvMenu () {

        System.out.println("********** INVENTORY **********");
        System.out.println("OPTION P:   PICK UP");
        System.out.println("OPTION D:   RETURN");
        System.out.println("OPTION C:   CHECK OUT");
        System.out.println("OPTION E:   EXIT");

        System.out.println(" ");
        System.out.println("Please enter a command.");

    }

    //-------------------------------------------------------------------
    // The command prompts for Inventory.
    //-------------------------------------------------------------------

    private static void InvCmd (Customer cust, Inventory[] inv, Scanner read) {

        char cmd, turn, pur;
        InvMenu();

        cmd = read.next().charAt(0);
        cmd = Character.toUpperCase(cmd);

        while (cmd != 'E') {

            switch (cmd) {

                case 'P' : // Pickup

                    //-------------------------------------------------------------------
                    // Switch case for viewing everything in the store,
                    // Then putting said item into cart if need be.
                    // Should be posting five per page.
                    //-------------------------------------------------------------------

                    System.out.println("********** ITEMS IN STOCK **********");
                    System.out.println(" ");

                    for (int i = 0; i < inv.length; i++) {
                        if (inv[i].price > 0)
                            System.out.println(inv[i].price + "         "
                                    + inv[i].item + "           " + inv[i].item_d);

                        if ((i+1) % 5 == 0 ) {
                            /*
                            previously i % 5, want to print 5 per page, i starts with 0
                            to get five per page, start at 1 instead
                            use parenthesis because modulus has precedence over addition
                            */
                            System.out.println(" ");
                            System.out.println("TURN PAGE? Y/N");
                            turn = read.next().charAt(0);
                            turn = Character.toUpperCase(turn);

                            //-------------------------------------------------------------------
                            // Check if customer will be purchasing.
                            //-------------------------------------------------------------------

                            if (turn != 'Y') {
                                System.out.println("Would you like to make a purchase? Y/N");
                                pur = read.next().charAt(0);
                                pur = Character.toUpperCase(pur);

                                if (pur == 'Y') {
                                    // some method call here
                                } // If purchase

                                else {
                                    i = (inv.length + 2);
                                    System.out.println("Returning you to the Inventory Menu.");
                                } // Else

                            } // If (turn is No, or anything that isn't yes.)
                        } // If (i % 5)
                    } // For loop

                    break;

                case 'D' : // Return
                    break;

                case 'C' : // Check Out
                    break;

                default :
                    System.out.println("That is not a proper option.");
                    System.out.println("Please try again.");

            } // Switch for Inventory

            System.out.println(" ");
            InvMenu();
            cmd = read.next().charAt(0);
            cmd = Character.toUpperCase(cmd);

        } // While for Inventory
    } // Method for InvCmd()

} // Main
